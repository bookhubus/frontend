import React, { Component } from 'react';
import LogoSvg from '../assets/svg/logo'
import HomeSvg from '../assets/svg/home'
import DarkIcon from '../assets/svg/darkIcon'
import DiscoverSvg from '../assets/svg/discover'
import LibararySvg45 from '../assets/svg/library45'
import ChallengeSvg from '../assets/svg/challengeSvg'
import './navbar.css'
import ProfileSVG from "../assets/svg/profile"

class NavBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPage: this.props.pageName,
            darkMode: false
        }
        this.handleClick = this.handleClick.bind(this)
        this.darkModeClicked = this.darkModeClicked.bind(this)
    }
    handleClick(event) {
        console.log(event.target)

        const name = event.target.getAttribute('name')
        console.log(name)
        this.setState({
            currentPage: name,
        })
    }
    darkModeClicked() {
        this.setState({
            darkMode: !this.state.darkMode
        })
    }
    render() {
        const { currentPage } = this.state
        return (
            <div className="NavBarContaier">
                <div className="labelContainer">
                    <LogoSvg />
                    <a
                        className="logo"
                        onClick={this.darkModeClicked}>
                        <DarkIcon className="l" />
                    </a>
                </div>
                <div className="menuContainer">
                    <a
                        href="/"
                        name="home"
                        onClick={this.handleClick}
                        style={currentPage === "home" ? { color: '#5FC56A' } : {color:'black'}}
                    >
                        <HomeSvg
                            name='home'
                            className="svgStyle"
                            stroke={currentPage === "home" ? "#5FC56A" : "black"}
                        />
                        Home
                    </a>
                    <br />
                    <a
                        href="/discover"
                        name="discover"
                        style={currentPage === "discover" ? { color: '#5FC56A' } : {color:'black'}}
                        onClick={this.handleClick}
                    >
                        <DiscoverSvg
                            name="discover"
                            className="svgStyle"
                            stroke={currentPage === "discover" ? "#5FC56A" : "black"}
                        />
                        Discover
                    </a>
                    <br />
                    <a
                        href="/library"
                        name="library"
                        style={currentPage === "library" ? { color: '#5FC56A' } : {color:'black'}}
                        onClick={this.handleClick}
                    >
                        <LibararySvg45
                            name="library"
                            className="svgStyle"
                            stroke={currentPage === "library" ? "#5FC56A" : "black"}
                        />
                        My Library
                    </a>
                    <br />
                    {/* <a
                        href="/lists"
                        name="list"
                        style={currentPage === "list" ? { color: '#5FC56A' } : {color:'black'}}
                        onClick={this.handleClick}
                    >
                        <ListSvg
                            name="list"
                            className="svgStyle"
                            stroke={currentPage === "list" ? "#5FC56A" : "black"}
                        />
                        Lists
                    </a>
                    <br /> */}
                    <a
                        href="/challenge"
                        name="challenge"
                        style={currentPage === "challenge" ? { color: '#5FC56A' } : {color:'black'}}
                        onClick={this.handleClick}
                    >
                        <ChallengeSvg
                            name="challenge"
                            className="svgStyle"
                            stroke={currentPage === "challenge" ? "#5FC56A" : "black"}
                        />
                        Challenges
                    </a>
                    <br />
                    {/* <a
                        href="/profile"
                        name="profile"
                        style={currentPage === "profile" ? { color: '#5FC56A' } : {color:'black'}}
                        onClick={this.handleClick}
                    >
                        <ProfileSVG
                        style= {{marginLeft: "3px"}}
                            name="profile"
                            className="svgStyle"
                            stroke={currentPage === "profile" ? "#5FC56A" : "black"}
                        />
                        Profile
                    </a> */}
                </div>
            </div>
        )
    }
}

export default NavBar;
