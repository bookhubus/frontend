import React, { Component, useState } from 'react';
import NavBar from '../../navigationbar/NavBar';
import './style.css'
import user from '../../assets/image/user.jpg'
import bookCover from '../../assets/image/bookCover.jpg'
import BookList from '../../components/bookList/bookList'
import Post from '../../components/post/post'
import Library24SVG from "../../assets/svg/library24"
import Congrate from "../../assets/svg/congrate"
import { useQuery, gql, useMutation } from '@apollo/client';
import { useHistory } from "react-router-dom";


var books2 = [{
  bookImg: bookCover,
  genere: "Fiction",
  rating: "9.7",
  bookName: "Harry Potter and the Prisoner of Azkaban",
  percent: "66",
}]

var challenges = [{
  challengeName: "Hundered Book Challenge",
  privacy: "private",
  timeLeft: "4 days",
  booksNum: "21",
  friendImg1: user,
  percentage1: "75",
  friendImg2: user,
  percentage2: "75",
  friendImg3: user,
  percentage3: "75",
  friendImg4: user,
  percentage4: "75",
}]

const graphQLQuery = gql`query GetReadBooks {
  me
  {
    id
    name
    image
  }
  library
  {
    edges
    {
      node
      {
        id
        name
        coverImage
        genre
        author
        {
          name
        }
        progress
        {
          percentage
        }
      }
    }
  }
}`

const getTimeline = gql`query GetTimeline {
  timeline
  {
    count
    edges
    {
      node
      {
        id
        text
        author
      }
    }
  }
}`

const getChallengesQuery = gql`query GetChallenges {
  me
  {
    id
    challenges
    {
      count
      edges
      {
        node
        {
          id
          name
          duration
          creator
          {
            id
            name
          }
          books
          {
            id
          }
          users
          {
            id
            image
          }
        }
      }
    }
  }
}`;

const Home = (props) => {
  let history = useHistory();
  if (!localStorage.getItem("userToken"))
    history.replace({ pathname: "/login" })


  const [booksCategory, setBooksCategory] = useState("Popular")
  const { loading, error, data } = useQuery(graphQLQuery);
  const { loading:loading2, error:error2, data:data2 } = useQuery(getTimeline);
  const { loading:loading3, error:error3, data:data3 } = useQuery(getChallengesQuery);

  if (loading || loading2 || loading3) return <p>Loading...</p>;
  if (error) console.log("ERROR1: " + error.message);
  if (error2) console.log("ERROR2: " + error2.message);
  if (error3) console.log("ERROR3: " + error3.message);

  if (data) {
    localStorage.setItem('image', data.me.image)

    books2 = [];
    let dataExtracted = data.library.edges;
    let i = 0;
    for(; i < dataExtracted.length; i++)
    {
      if(dataExtracted[i].node.progress)
        books2.push({bookImg: dataExtracted[i].node.coverImage??bookCover, genere: dataExtracted[i].node.genre, rating: "9.7", bookName: dataExtracted[i].node.name, percent: dataExtracted[i].node.progress?.percentage??0, bookId: dataExtracted[i].node.id});
    }
    for (; i < 4; i++)
    {
      books2.push({bookImg: "https://listimg.pinclipart.com/picdir/s/140-1402686_clock-clipart-animation-clock-cartoon-png-gif-transparent.png", genere: "Fantasy", rating: "9.7", bookName: "", percent: 0});
    }
  }

  var posts = []
  if(data2) {
    console.log(data2);
    let i = 0;
    for(; i < data2.length; i++)
    {
      posts.push({postCreator: data2.author, postText: data2.text, postDate: "N/A", hasChallengeInfo: false, footerIcon: <Library24SVG/>, footerText: "Suggest a book to read", hasSecondStat: false});
    }
  }

  if(data3)
  {
    console.log(data3);
    challenges = []
    let dataExtracted = data3.me.challenges.edges;
    let i = 0;
    for(; i < dataExtracted.length; i++)
    {
      challenges.push({challengeName: dataExtracted[i].node.name, privacy: "private", timeLeft: dataExtracted[i].node.duration, startDate: dataExtracted[i].node.start, booksNum: dataExtracted[i].node.books?.length??0, 
        friendImg1: dataExtracted[i].node.users[0].image??user, percentage1: 75, friendImg2: user, percentage2: 70, friendImg3: user, percentage3: 60, friendImg4: user, percentage4: 30});
    }
  }

  return (
    <div className="homeContainer">
      <NavBar pageName={props.pageName} />
      <div className="homeStyle">
        <div className="homeHeader">
          <div className="textContainer">
            <div className="homeText" style={{ fontWeight: "bold" }}>Hello, {localStorage.getItem("name")}</div>
            <div className="homeText">Continue Reading</div>
          </div>
          <img className="homeUser" src={localStorage.getItem("image")} />
        </div>
        <div style={{ marginTop: "15px", height: "194px" }}>
          <BookList cards={books2} bookStyle={"bookCover"} isBookCard={true} hasInfo={true} />
        </div>
        <div style={{ marginTop: "33px", height: "194px" }}>
          <BookList cards={challenges} isBookCard={false} />
        </div>
        <div style={{ marginTop: "60px", marginBottom: "100px", marginLeft: "150px" }}>
          {posts}
        </div>
      </div>
    </div>
  );
}

export default Home
