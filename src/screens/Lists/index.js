import React, { Component } from 'react';

import '../../components/header/header.css'
import NavBar from '../../navigationbar/NavBar';
import ArrowRightCircleSVG from '../../assets/svg/arrow_right_circle.js';
import bookCover from '../../assets/image/bookCover.jpg'
import './style.css';
import BooksList from '../../components/bookList/bookList'
import Header from '../../components/header/header'
import CardViewer from '../../components/cardViewer/cardViewer'

const cards = [
  {
    bookImg: bookCover,
    bookName: "Harry Potter and the Prisoner of Azkaban",
    Author: "J. K. Rowling",
    description: "During his third year at Hogwarts School for Witchcraft and Wizardry, Harry Potter must confront the devious and dangerous wizard responsible for his parents' deaths.",
    publishDate: "21 March, 2020",
    genre: "fiction",
    part: "Part 3 Of a 6 Books Series",
    price: "25.5$",
    rating: "3",
    percent: 75,
    friends: [bookCover, bookCover, bookCover, bookCover],
    friendsNumber: 4
  },
  {
    bookImg: bookCover, bookName: "Harry Potter and the Prisoner of Azkaban",
    Author: "J. K. Rowling",
    description: "During his third year at Hogwarts School for Witchcraft and Wizardry, Harry Potter must confront the devious and dangerous wizard responsible for his parents' deaths.",
    publishhDate: "21 March, 2020",
    genre: "fiction",
    part: "Part 3 Of a 6 Books Series",
    price: "25.5$",
    rating: "3",
    percent: 75,
    friends: [bookCover, bookCover, bookCover, bookCover],
    friendsNumber: 4
  },
  {
    bookImg: bookCover, bookName: "Harry Potter and the Prisoner of Azkaban",
    Author: "J. K. Rowling",
    description: "During his third year at Hogwarts School for Witchcraft and Wizardry, Harry Potter must confront the devious and dangerous wizard responsible for his parents' deaths.",
    publishhDate: "21 March, 2020",
    genre: "fiction",
    part: "Part 3 Of a 6 Books Series",
    price: "25.5$",
    rating: "3",
    percent: 75,
    friends: [bookCover, bookCover, bookCover, bookCover],
    friendsNumber: 4
  },
  {
    bookImg: bookCover, bookName: "Harry Potter and the Prisoner of Azkaban",
    Author: "J. K. Rowling",
    description: "During his third year at Hogwarts School for Witchcraft and Wizardry, Harry Potter must confront the devious and dangerous wizard responsible for his parents' deaths.",
    publishhDate: "21 March, 2020",
    genre: "fiction",
    part: "Part 3 Of a 6 Books Series",
    price: "25.5$",
    rating: "3",
    percent: 75,
    friends: [bookCover, bookCover, bookCover, bookCover],
    friendsNumber: 4
  },
  {
    bookImg: bookCover, bookName: "Harry Potter and the Prisoner of Azkaban",
    Author: "J. K. Rowling",
    description: "During his third year at Hogwarts School for Witchcraft and Wizardry, Harry Potter must confront the devious and dangerous wizard responsible for his parents' deaths.",
    publishhDate: "21 March, 2020",
    genre: "fiction",
    part: "Part 3 Of a 6 Books Series",
    price: "25.5$",
    rating: "3",
    percent: 75,
    friends: [bookCover, bookCover, bookCover, bookCover],
    friendsNumber: 4
  }
]

class Lists extends Component {

  constructor() {
    super()
    this.state = {
      booksCategory: "Popular",
      searchText: ""
    }
    this.search = this.search.bind(this)
  }
  changeCategory(cat) {
    this.setState({
      booksCategory: cat
    })
  }
  search(text) {
    this.setState({
      searchText: text
    })
  }
  render() {
    const { booksCategory } = this.state
    return (
      <div className="discoverContainer">
        <NavBar pageName = {this.props.pageName}/>
        <div className="discoverStyle">
          <Header userName={"Jony"} />
          <CardViewer cards={cards} listContainer={"virticalListContainer"} />
          <div className="suggestions">
            <div className="listHeader">
              <div className="forYou">
                For You
              </div>
              <div className="seeMoreContainer">
                <a className="seeMore">
                  See More
                  <ArrowRightCircleSVG />
                </a>
              </div>
            </div>
            <div>
              <div style={{ marginTop: "2.3%" }}>
                <BooksList cards={cards} isBookCard = {true} hasInfo = {false}/>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Lists
