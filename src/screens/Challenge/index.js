import React, { Component, useState } from 'react';
import NavBar from '../../navigationbar/NavBar';
import './style.css'
import user from '../../assets/image/user.jpg'
import BookList from '../../components/bookList/bookList'
import AddButton from '../../components/addButton/addButton'
import ChallengeVirticalList from '../../components/challengeVirticalList/challengeVirticalList'
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import CreateChallenge from '../../components/forms/CreateChallenge'
import { gql, useQuery } from '@apollo/client';
import { useHistory } from 'react-router-dom';
import SearchSVG from '../../assets/svg/search.js'
import bookimg from '../../assets/image/bookCover.jpg'
import userImg from '../../assets/image/user.jpg'


let challenges = [
  {
    challengeName: "Hundered Book Challenge",
    privacy: "private",
    timeLeft: "4 days",
    booksNum: "21",
    friendImg1: user,
    percentage1: "75",
    friendImg2: user,
    percentage2: "75",
    friendImg3: user,
    percentage3: "75",
    friendImg4: user,
    percentage4: "75",
  }]
const AssemISAFuckingDipshit= 
[
  {
    id: '1',
    name: "assem sucks kekw he is a bitch and we all hate him so much he is bad bad bad",
    src: bookimg
  },
  {
    id: '1',
    name: "assem sucks kekw",
    src: bookimg
  },
  {
    id: '1',
    name: "assem sucks kekw",
    src: bookimg
  },
  {
    id: '1',
    name: "assem sucks kekw",
    src: bookimg
  }
]
const AssemIsAmoreon= 
[
  {
    id: '1',
    Name: "AssemSucks",
    Img: userImg
  },
  {
    id: '2',
    Name: "AssemSucks",
    Img: userImg
  },
  {
    id: '3',
    Name: "AssemSucks",
    Img: userImg
  }
]

const getChallengesQuery = gql`query GetChallenges {
  me
  {
    id
    challenges
    {
      count
      edges
      {
        node
        {
          id
          name
          duration
          creator
          {
            id
            name
          }
          books
          {
            id
          }
          users
          {
            id
            image
          }
        }
      }
    }
  }
}`;

const Challenge = (props) => {
  let history = useHistory();
  if (!localStorage.getItem("userToken"))
    history.replace({ pathname: "/login" })

  const [booksCategory, setCategory] = useState("Popular")
  const [show, setShow] = useState(false)
  const [secPage, setsecPage] = useState(false)
  const [thirdPage, setThirdPage] = useState(false)

  const [searchText, setSearchText] = useState("")
  const { loading:loading3, error:error3, data:data3 } = useQuery(getChallengesQuery);

  const handleClose = React.useCallback(async () => {
    setShow(false);
  });

  const search =  React.useCallback(async (value) => {
    setSearchText(value)});
  const handleShow = React.useCallback(async () => {
    setShow(true);
    setsecPage(false);
    setThirdPage(false);
  });
  const handleSecNext = React.useCallback(async () => {
    setsecPage(true);
  });
  const handleThirdNext = React.useCallback(async () => {
    setThirdPage(true);
  });
  if(loading3) return <p>Loading...</p>;
  if (error3) console.log("ERROR3: " + error3.message);

  if(data3)
  {
    challenges = []
    let dataExtracted = data3.me.challenges.edges;
    let i = 0;
    for(; i < dataExtracted.length; i++)
    {
      challenges.push({challengeName: dataExtracted[i].node.name, privacy: "private", timeLeft: dataExtracted[i].node.duration, startDate: dataExtracted[i].node.start, booksNum: dataExtracted[i].node.books?.length??0, 
        friendImg1: dataExtracted[i].node.users[0].image??user, percentage1: 75, friendImg2: user, percentage2: 70, friendImg3: user, percentage3: 60, friendImg4: user, percentage4: 30});
    }
  }

  return (

    <div className="homeContainer">
      <NavBar pageName={props.pageName} />
      <div className="homeStyle">
        <div className="homeHeader">
          <div className="textContainer">
            <div className="homeText" style={{ fontWeight: "bold" }}>Hello, {localStorage.getItem("name")}</div>
            <div className="homeText">Your Challenges</div>
          </div>
          <img className="homeUser" src={user} />
        </div>
        <div style={{ marginTop: "33px", height: "194px" }}>
          <BookList cards={challenges} isBookCard={false} />
        </div>
        <br />
        <div className="homeText">Available Challenges</div>
        <ChallengeVirticalList challenges={challenges} />
      </div>
      <AddButton onClick={handleShow} />
        <>
         <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            {console.log(secPage,thirdPage)}
            <h3> {secPage? "AddBooks" : thirdPage? "AddUsers": "Create Challenge "} </h3>
          </Modal.Header>
          {secPage?
          <div> 
          <div className="searchField">
          <SearchSVG className="searchIcon" />
              <input
                  onChange={(value) => { search(value.target.value)}}
                  className="inputSearch"
                  placeholder="Search"
                  value={searchText} />
          </div>
          <div style={{height:'70vh', overflow: 'scroll',}}>
            <ul class="list-group" style={{margin: "10px", overflow: 'scroll'}}>
              {AssemISAFuckingDipshit.map((obj)=> 
              <li class="list-group-item" style={{textAlign: 'top'}}>
                <img src = {obj.src} style = {{ float: 'left', width:'100px', height: '140px', borderRadius: '7px', marginTop: '20px'}}/>
                <label style = {{ margin: '10px',fontFamily: 'cairo',fontWeight: 'bold',fontSize: '28px',lineHeight: '40px', maxWidth: ' 70%'}}>{obj.name}</label>
                <button className = "btn btn-secondary" style= {{float: 'right', marginTop: '50px'}}>
                Add Book
                </button>
              </li>
              )
              }
            </ul>
          </div>
          </div>
          :
          thirdPage?
          <div> 
          <div className="searchField">
          <SearchSVG className="searchIcon" />
              <input
                  onChange={(value) => { search(value.target.value)}}
                  className="inputSearch"
                  placeholder="Search"
                  value={searchText} />
          </div>
          <div style={{height:'70vh', overflow: 'scroll',}}>
            <ul class="list-group" style={{margin: "10px", overflow: 'scroll'}}>
              {AssemIsAmoreon.map((obj)=> 
              <li class="list-group-item" style={{textAlign: 'top'}}>
                <img src = {obj.Img} style = {{ float: 'left', width:'80px', height: '100px', borderRadius: '7px', marginTop: '10px'}}/>
                <label style = {{ margin: '10px',fontFamily: 'cairo',fontWeight: 'bold',fontSize: '28px',lineHeight: '40px', maxWidth: ' 70%'}}>{obj.Name}</label>
                <button className = "btn btn-secondary" style= {{float: 'right', marginTop: '50px'}}>
                Add Book
                </button>
              </li>
              )
              }
            </ul>
          </div>
          </div>
          :
          <CreateChallenge />
          }
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="success" onClick={secPage? handleClose : thirdPage? handleSecNext: handleThirdNext}>
              { secPage || thirdPage? "Save changes": "Next"}
            </Button>
          </Modal.Footer>
        </Modal>
      </>
        
    </div>
  );
}

export default Challenge
