import React, { Component } from 'react';
import NavBar from '../../navigationbar/NavBar';
import user from '../../assets/image/user.jpg';
import './style.css'
import DiscoverVirticalList from '../../components/discoverVirticalList/discoverVirticalList';
import { useHistory } from 'react-router-dom';

const cards = [
    {
        img: user,
        postText: "Wendy Jones just finished Harry Potter and the Prisoner of Azkaban",
        postDate: "5 Days ago",
        hasChallenge: true,
        challengeName: "Hundered Book Challenge",
        challengePrivacy: "private",
        challengeTimeLeft: "5 Days",
        bookNum: "2",
        footerIcon: "suggest",
        footerText: "Suggest a book to read",
        hasSecondState: true,
        footerText2: "Congrate"
    }
]

class Profile extends Component {
    constructor() {
        super()

        let history = useHistory();
        if (!localStorage.getItem("userToken"))
            history.replace({ pathname: "/login" })

        this.state = {
            editMode: false,
            userName: "",
            email: "",
            validEmail: true,
            number: "",
            validNumber: true,
            validPassword: true,
            newPassword: "",
            confirmedPassword: "",
            validConfirmedPassword: true,
            error: false,
            selectedImage: user
        }
        this.handleChange = this.handleChange.bind(this)
        this.editToggle = this.editToggle.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.fileChangedHandler = this.fileChangedHandler.bind(this)
    }
    handleChange(event) {
        const { name, value } = event.target
        this.setState(
            {
                [name]: value,
            }
        )
    }
    editToggle() {
        this.setState({
            editMode: !this.state.editMode,
            selectedImage: user
        })
    }
    async handleSubmit() {
        this.setState({
            validEmail: true,
            validNumber: true,
            validPassword: true,
            validConfirmedPassword: true,
            error: false
        })
        var email_check =
            RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
        if (!email_check.test(this.state.email)) {
            this.setState({
                validEmail: false,
                error: true
            })
        }
        var num_check =
            RegExp(/^01[12540]\d{8}$/i);
        if (await !num_check.test(this.state.number)) {
            this.setState({
                validNumber: false,
                error: true
            })
        }
        //pass must be atleast 6 char including cap and num and atmost 20 char
        var newPass_check =
            RegExp(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/);
        if (await !newPass_check.test(this.state.newPassword)) {
            this.setState({
                validPassword: false,
                error: true
            })
        }
        if (this.state.confirmedPassword !== this.state.newPassword) {
            this.setState({
                validConfirmedPassword: false,
                error: true
            })
        }
        if (!this.state.error) {
            //call API
            console.log(this.state.error)
            this.setState({
                editMode: false
            })
        }
    }
    fileChangedHandler = (event) => {
        const file = URL.createObjectURL(event.target.files[0]);
        this.setState({ selectedImage: file });
    }
    render() {
        const { newPassword, confirmedPassword, userName, email, number } = this.state
        return (
            <div className="homeContainer">
                <NavBar pageName = {this.props.pageName} />
                <div className="discoverStyle">
                    <div className="profileContainer">
                        <label className="homeUser"
                            style={{ position: "absolute", zIndex: "1", margin: "0" }}
                            for="image_pick">
                            <img
                                className="homeUser"
                                style ={{ cursor: "pointer" }}
                                src={this.state.selectedImage} />
                            <input type="file" accept="image/*" className="filetype" id="image_pick" onChange={this.fileChangedHandler} />
                        </label>

                        <div className="profileInfo">

                            {!this.state.editMode ?
                                <div>
                                    <div className="profileText" style={{ marginTop: "70px" }}>Rawda Tarek Fathy</div>
                                    <div className="profileText">rawda.t98@gmail.com</div>
                                    <div className="profileText">011147582</div>
                                    <button className="editButton" onClick={this.editToggle}>Edit</button>
                                </div>
                                :
                                <div>
                                    <div className="fieldRow" style={{ marginTop: "70px" }}>
                                        <input
                                            placeholder={"Name"}
                                            type="text"
                                            className="textInput"
                                            minLength="8"
                                            name="userName"
                                            value={userName}
                                            onChange={this.handleChange}
                                            required={true}
                                        />
                                        <input
                                            placeholder={"New Password"}
                                            style={this.state.validPassword ? {} : { borderColor: 'red' }}
                                            type="password"
                                            className="textInput"
                                            minLength="8"
                                            name="newPassword"
                                            value={newPassword}
                                            onChange={this.handleChange}
                                            required={true}
                                        />
                                    </div>
                                    <div className="fieldRow">
                                        <input
                                            placeholder={"Email"}
                                            style={this.state.validEmail ? {} : { borderColor: 'red' }}
                                            type="email"
                                            className="textInput"
                                            minLength="8"
                                            name="email"
                                            value={email}
                                            onChange={this.handleChange}
                                            required={true}
                                        />
                                        <input
                                            placeholder={"Confirm Password"}
                                            style={this.state.validConfirmedPassword ? {} : { borderColor: 'red' }}
                                            type="password"
                                            className="textInput"
                                            minLength="8"
                                            name="confirmedPassword"
                                            value={confirmedPassword}
                                            onChange={this.handleChange}
                                            required={true}
                                        />
                                    </div>
                                    <div className="fieldRow">
                                        <input
                                            placeholder={"Number"}
                                            style={this.state.validNumber ? {} : { borderColor: 'red' }}
                                            type="text"
                                            className="textInput"
                                            minLength="8"
                                            name="number"
                                            value={number}
                                            onChange={this.handleChange}
                                            required={true}
                                        />
                                        <div style={{ display: "flex", justifyContent: "space-between", width: "180px", alignSelf: "center" }}>
                                            <button
                                                className="editButton"
                                                style={{ marginTop: "5px" }}
                                                onClick={this.handleSubmit}>Save
                                                </button>
                                            <button
                                                className="editButton"
                                                style={{ marginTop: "5px" }}
                                                onClick={this.editToggle}>Cancel
                                                </button>
                                        </div>
                                    </div>
                                </div>
                            }
                        </div>
                        <div className="profilePosts">
                            <DiscoverVirticalList items={cards} isBookCard={false} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Profile
