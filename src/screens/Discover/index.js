import React, { Component, useState } from 'react';

import NavBar from '../../navigationbar/NavBar';
import ArrowRightCircleSVG from '../../assets/svg/arrow_right_circle.js';
import bookCover from '../../assets/image/bookCover.jpg'
import './style.css';
import BooksList from '../../components/bookList/bookList.js'
import DiscoverVirticalList from '../../components/discoverVirticalList/discoverVirticalList'
import Header from '../../components/header/header'
import { gql, useQuery } from '@apollo/client';
import { useHistory } from 'react-router-dom';

let books1 = [
  { bookImg: bookCover },
  { bookImg: bookCover },
  { bookImg: bookCover },
  { bookImg: bookCover },
  { bookImg: bookCover },
  { bookImg: bookCover },
  { bookImg: bookCover },
  { bookImg: bookCover },
  { bookImg: bookCover },
]



let cards = [{
  bookImg: bookCover,
  bookName: "Harry Potter and the Prisoner of Azkaban",
  Author: "J. K. Rowling",
  publishDate: "21 March, 2020",
  genre: "fiction",
  part: "Part 3 Of a 6 Books Series",
  price: "25.5$"
},
{
  bookImg: bookCover, bookName: "Harry Potter and the Prisoner of Azkaban",
  Author: "J. K. Rowling",
  publishhDate: "21 March, 2020",
  genre: "fiction",
  part: "Part 3 Of a 6 Books Series",
  price: "25.5$"
},
{
  bookImg: bookCover, bookName: "Harry Potter and the Prisoner of Azkaban",
  Author: "J. K. Rowling",
  publishhDate: "21 March, 2020",
  genre: "fiction",
  part: "Part 3 Of a 6 Books Series",
  price: "25.5$"
},
{
  bookImg: bookCover, bookName: "Harry Potter and the Prisoner of Azkaban",
  Author: "J. K. Rowling",
  publishhDate: "21 March, 2020",
  genre: "fiction",
  part: "Part 3 Of a 6 Books Series",
  price: "25.5$"
},
{
  bookImg: bookCover, bookName: "Harry Potter and the Prisoner of Azkaban",
  Author: "J. K. Rowling",
  publishhDate: "21 March, 2020",
  genre: "fiction",
  part: "Part 3 Of a 6 Books Series",
  price: "25.5$"
}]

const getRecommendedQuery = gql`query GetRecommended{
  recommendedBooksForMe
  {
    edges
    {
			node
      {
        id
        coverImage
      }
    }
  }
}`

const getNewestQuery = gql`query GetNewest{
  newestBooks
  {
    edges
    {
			node
      {
        id
        name
        author
        {
          name
        }
        genre
        coverImage
      }
    }
  }
  popularBooks
  {
    edges
    {
			node
      {
        id
        name
        author
        {
          name
        }
        genre
        coverImage
      }
    }
  }
}`;

const Discover = (props) => {
  let history = useHistory();
  if (!localStorage.getItem("userToken"))
    history.replace({ pathname: "/login" })

  const [ booksCategory, setBooksCategory ] = useState("Popular");
  const { loading, error, data } = useQuery(getRecommendedQuery);
  const { loading:loading2, error:error2, data:data2 } = useQuery(getNewestQuery);

  let newest = [], popular = [], featured = [];

  const changeCategory = React.useCallback(async (cat) => {
    setBooksCategory(cat);
    if(cat === "Newest")
      cards = newest;
    else if(cat === "Popular")
      cards = popular;
    else
      cards = featured;
  });

  if (loading || loading2) return <p>Loading...</p>;
  if (error) console.log("ERROR: " + error.message);
  if (error2) console.log("ERROR2: " + error2.message);

/*{
  bookImg: bookCover,
  bookName: "Harry Potter and the Prisoner of Azkaban",
  Author: "J. K. Rowling",
  publishDate: "21 March, 2020",
  genre: "fiction",
  part: "Part 3 Of a 6 Books Series",
  price: "25.5$"
},*/

  if(data)
  {
    books1 = [];
    let dataExtracted = data.recommendedBooksForMe.edges;
    for(let i = 0; i < dataExtracted.length; i++)
    {
      books1.push({bookImg: dataExtracted[i].node.coverImage, bookId: dataExtracted[i].node.id});
    }
  }

  if(data2)
  {
    console.log(data2);
    newest = [];
    popular = [];
    featured = [];
    let dataNewest = data2.newestBooks.edges;
    let dataPopular = data2.popularBooks.edges;
    for(let i = 0; i < dataNewest.length; i++)
    {
      newest.push({price: "25.5$", part: "Part 3 Of a 6 Books Series", genre: dataNewest[i].node.genre, publishDate: "Today", author: dataNewest[i].node.author, bookImg: dataNewest[i].node.coverImage, bookName: dataNewest[i].node.name, bookId: dataNewest[i].node.id});
    }
    for(let i = 0; i < dataPopular.length; i++)
    {
      popular.push({price: "25.5$", part: "Part 3 Of a 6 Books Series", genre: dataPopular[i].node.genre, publishDate: "Today", author: dataPopular[i].node.author, bookImg: dataPopular[i].node.coverImage, bookName: dataPopular[i].node.name, bookId: dataPopular[i].node.id});
      featured.push({price: "25.5$", part: "Part 3 Of a 6 Books Series", genre: dataPopular[i].node.genre, publishDate: "Today", author: dataPopular[i].node.author, bookImg: dataPopular[i].node.coverImage, bookName: dataPopular[i].node.name, bookId: dataPopular[i].node.id});
    }
  }

  return (
    <div className="discoverContainer">
      <NavBar pageName={props.pageName} />
      <div className="discoverStyle">
        <Header userName={"Jony"} />
        <div className="suggestions">
          <div className="listHeader">
            <div className="forYou">
              For You
              </div>
            <div className="seeMoreContainer">
              <a className="seeMore">
                See More
                  <ArrowRightCircleSVG />
              </a>
            </div>
          </div>
          <div>
            <div style={{ marginTop: "2.3%" }}>
              <BooksList cards={books1} isBookCard={true} hasInfo={false} />
            </div>
          </div>
        </div>
        <div className="discoverSectionContainer">
          <a
            className="discoverSectionText"
            style={booksCategory === "Newest" ? { fontWeight: "bold" } : {}}
            onClick={() => { changeCategory('Newest') }}>
            Newest</a>
          <a
            className="discoverSectionText"
            style={booksCategory === "Popular" ? { fontWeight: "bold" } : {}}
            onClick={() => changeCategory('Popular')}>
            Popular</a>
          <a
            className="discoverSectionText"
            style={booksCategory === "Featured" ? { fontWeight: "bold" } : {}}
            onClick={() => changeCategory('Featured')}>
            Featured</a>
        </div>
        <DiscoverVirticalList items={cards} listContainer={"virticalListContainer"} isBookCard={true} />
      </div>
    </div>
  );
}

export default Discover
