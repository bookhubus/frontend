import React, { Component, useState } from 'react';
import NavBar from '../../navigationbar/NavBar';
import './style.css'
import bookCover from '../../assets/image/bookCover.jpg'
import BookList from '../../components/bookList/bookList';
import ReactStars from "react-rating-stars-component";
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { useParams, useHistory } from 'react-router-dom';
import { gql, useQuery, useMutation } from '@apollo/client';

let mainBook = {
    bookName: "Harry Potter and the Prisoner of Azkaban",
    rating: "9.7",
    genre: "Fiction",
    bookImg: bookCover,
    author: "test",
    overview: "overview"
}

let books2 = [{
    bookImg: bookCover,
    genere: "Fiction",
    rating: "9.7",
    bookName: "Harry Potter and the Prisoner of Azkaban",
    percent: "66",
},
{
    bookImg: bookCover,
    genere: "Fiction",
    rating: "9.7",
    bookName: "Harry Potter and the Prisoner of Azkaban",
    percent: "66",
},
{
    bookImg: bookCover,
    genere: "Fiction",
    rating: "9.7",
    bookName: "Harry Potter and the Prisoner of Azkaban",
    percent: "66",
},
{
    bookImg: bookCover,
    genere: "Fiction",
    rating: "9.7",
    bookName: "Harry Potter and the Prisoner of Azkaban",
    percent: "66",
},
{
    bookImg: bookCover,
    genere: "Fiction",
    rating: "9.7",
    bookName: "Harry Potter and the Prisoner of Azkaban",
    percent: "66",
},
{
    bookImg: bookCover,
    genere: "Fiction",
    rating: "9.7",
    bookName: "Harry Potter and the Prisoner of Azkaban",
    percent: "66",
},
{
    bookImg: bookCover,
    genere: "Fiction",
    rating: "9.7",
    bookName: "Harry Potter and the Prisoner of Azkaban",
    percent: "66",
},
{
    bookImg: bookCover,
    genere: "Fiction",
    rating: "9.7",
    bookName: "Harry Potter and the Prisoner of Azkaban",
    percent: "66",
},
{
    bookImg: bookCover,
    genere: "Fiction",
    rating: "9.7",
    bookName: "Harry Potter and the Prisoner of Azkaban",
    percent: "66",
},
{
    bookImg: bookCover,
    genere: "Fiction",
    rating: "9.7",
    bookName: "Harry Potter and the Prisoner of Azkaban",
    percent: "66",
}]

const getBookQuery = gql`query GetBook($input: String!)
{
  book(id: $input)
  {
    id
    name
    genre
    slug
    author
    {
      name
    }
    coverImage
  }
}`;

const addBookToLibQuery = gql`mutation AddBookToLibrary($input: AddToLibraryInput!)
{
  addToLibrary(input: $input)
  {
    status
  }
}`;

const BookInfo = () => {
    let history = useHistory();
    if (!localStorage.getItem("userToken"))
      history.replace({ pathname: "/login" })

    const [booksCategory, setBooksCategory] = useState("Popular");
    const [rating, setRating] = useState("");
    const [show, setShow] = useState(false);

    const { id } = useParams();
    const { loading, error, data } = useQuery(getBookQuery, { variables: { input: id.toString() } } );
    const [addBookToLib, { loading:loading2, error:error2, data:data2 }] = useMutation(addBookToLibQuery);

    const handleClose = React.useCallback(async () => {
        setShow(false);
    });

    const handleShow = React.useCallback(async () => {
        setShow(true);
    });
    const ratingChanged = React.useCallback(async (newRating) => {
        setRating(newRating);
    });
    const addBookStart = React.useCallback(async () => {
        const data2 = await addBookToLib({
            variables: {
                input: {
                    bookId: id.toString()
                }
            }
        });
    });

    if (loading || loading2) return <p>Loading...</p>;
    if (error) console.log("ERROR: " + error);
    if (error2) console.log("ERROR2: " + error2);

    if (data) {
        let dataExtracted = data.book;
        mainBook = {
            bookName: dataExtracted.name,
            rating: "9.7",
            genre: dataExtracted.genre[0],
            bookImg: dataExtracted.coverImage ?? bookCover,
            author: dataExtracted.author.name,
            overview: dataExtracted.slug
        }
    }

    return (
        <div className="homeContainer">
            <NavBar />
            <div className="homeStyle">
                <div className="bookInfoContainer">
                    <div style={{ width: "210px" }}>
                        <img src={mainBook.bookImg} className="bookStyle" />
                        <button
                            className="editButton"
                            onClick={() => {
                                addBookStart();
                                handleShow();
                            }}
                            style={{ marginLeft: "60px", width: "110px" }}>Add to library
                                </button>
                    </div>
                    <div className="bookInfo">
                        <div className="rating">

                            <ReactStars
                                count={5}
                                onChange={ratingChanged}
                                size={23}
                                activeColor="#ffd700"
                            />
                            <div className="text" style={{ fontWeight: "lighter", marginLeft: "20px", marginTop: "-4px" }}>
                                {mainBook.rating}
                            </div>

                        </div>
                        <div className="text">
                            {mainBook.bookName}

                        </div>
                        <div className="text" style={{ fontSize: "21px", wordWrap: "break-word", lineHeight: "40px" }}>
                            {mainBook.author}
                        </div>
                        <div className="text" style={{ fontSize: "16px", wordWrap: "break-word", lineHeight: "30px" }}>
                            N/A
                        </div>
                        <div className="text" style={{ fontSize: "16px", wordWrap: "break-word", lineHeight: "30px" }}>
                            {mainBook.genre}
                        </div>
                        <div className="text" style={{ fontSize: "16px", wordWrap: "break-word", lineHeight: "30px", marginBottom: "30px" }}>
                            {mainBook.overview}
                        </div>
                    </div>


                </div>
                <div style={{ width: "90%" }}>
                    <div className="text" style={{ marginBottom: "20px", marginTop: "50px" }}>Similar Books</div>
                    <BookList cards={books2} bookStyle={"bookCover"} isBookCard={true} />
                </div>
            </div>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                </Modal.Header>
                <div className="text" style={{ display: "flex", alignSelf: "center", marginTop: "30px", marginBottom: "30px" }}>

                    Book Added Successfully!
                    </div>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                        </Button>

                </Modal.Footer>
            </Modal> 
        </div>
    );
}

export default BookInfo;
