import React, { Component, useState } from 'react';

import '../../components/header/header.css'
import SearchSVG from '../../assets/svg/search.js';
import NavBar from '../../navigationbar/NavBar';
import ArrowRightCircleSVG from '../../assets/svg/arrow_right_circle.js';
import bookCover from '../../assets/image/bookCover.jpg'
import './style.css';
import DiscoverHorizontalList from '../../components/discoverHorizontalList/discoverHorizontalList'
import Header from '../../components/header/header'
import BookList from '../../components/bookList/bookList';
import { gql, useQuery } from '@apollo/client';
import { useHistory } from 'react-router-dom';

var books = [
  {
    src: bookCover,
    width: 2,
    height: 3
  }
]

var cards = [{
  bookImg: bookCover,
  genere: "Fiction",
  rating: "9.7",
  bookName: "Harry Potter and the Prisoner of Azkaban",
  percent: "66",
}]

const graphQLQuery = gql`query GetReadBooks {
  library
  {
    edges
    {
      node
      {
        id
        name
        coverImage
        pagesCount
        genre
        author
        {
          name
        }
        progress
        {
          percentage
          pageNumber
        }
      }
    }
  }
}`;

const Library = (props) => {
  let history = useHistory();
  if (!localStorage.getItem("userToken"))
    history.replace({ pathname: "/login" })

  const { booksCategory, setBooksCategory } = useState("Popular");
  const { searchText, setSearchText } = useState("");
  const { loading, error, data } = useQuery(graphQLQuery);

 

  const changeCategory = React.useCallback(async (cat) => {
    setBooksCategory(cat);
  });

  const search = React.useCallback(async (text) => {
    setSearchText(text);
  });

  if(loading) return <p>Loading...</p>;
  /*{
    bookImg: bookCover,
    genere: "Fiction",
    rating: "9.7",
    bookName: "Harry Potter and the Prisoner of Azkaban",
    percent: "66",
  },*/
  if (data) {
    books = [];
    cards = [];
    let dataExtracted = data.library.edges;
    let i = 0;
    for(i = 0; i < dataExtracted.length; i++)
    {
      books.push({bookId: dataExtracted[i].node.id, src: dataExtracted[i].node.coverImage??bookCover, width: 1, height: 2});
      if(dataExtracted[i].node.progress)
      {
        cards.push({bookId: dataExtracted[i].node.id, bookImg: dataExtracted[i].node.coverImage??bookCover, genere: dataExtracted[i].node.genre, 
          rating: 9.4, bookName: dataExtracted[i].node.name, percent: dataExtracted[i].node.progress.percentage??0, currentPage: dataExtracted[i].node.progress.pageNumber??0,
           maxPages: dataExtracted[i].node.pagesCount??200 });
      }
    }

    for(; i < 6; i++)
    {
      books.push({ src:"https://listimg.pinclipart.com/picdir/s/140-1402686_clock-clipart-animation-clock-cartoon-png-gif-transparent.png", width: 1, height: 2});
      cards.push({bookId: "", bookCover, genere: "", 
        rating: 9.4, bookName: "", percent: 0, currentPage: 0, maxPages: 200});
    }
  }
  /*if (data) {
    cards = [];
    let dataExtracted = data.library.edges;
    for(let i = 0; i < dataExtracted.length; i++)
    {
      cards.push({bookImg: bookCover, genere: "Fantasy", rating: "9.7", bookName: dataExtracted[i].node.name, percent: dataExtracted[i].node.progress?.percentage??0});
    }
  }*/

  return (
    <div className="discoverContainer">
      <NavBar pageName={props.pageName} />
      <div className="discoverStyle">
        <Header userName={"Jony"} />
        <div className="suggestions">
          <div className="listHeader">
            <div className="forYou">
              Reading
              </div>
          </div>
          <div>
            <div style={{ marginTop: "2.3%" }}>
              <BookList cards={cards} bookStyle={"bookCover"} isBookCard={true} hasInfo={true} />
            </div>
          </div>
        </div>
        <div className="topBar">
          <div className="topBarUser">
            <div className="forYou">
              Collected
              </div>
          </div>
          <div className="searchField">
            <SearchSVG className="searchIcon" />
            <input
              onChange={(value) => search(value)}
              className="inputSearch"
              placeholder="Search"
              value={searchText} />
          </div>
        </div>
        <DiscoverHorizontalList books={books} listContainer={"horizontalListContainer"} />
      </div>
    </div>
  );
}

export default Library