import * as React from "react"

function SearchSVG(props) {
  return (
    <svg width={19} height={16} viewBox="0 0 19 16" fill="none" {...props}>
      <path
        clipRule="evenodd"
        d="M8.106 12c3.142 0 5.688-2.239 5.688-5s-2.546-5-5.688-5C4.965 2 2.418 4.239 2.418 7s2.547 5 5.688 5z"
        stroke="#1C1C1C"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M16.07 14l-3.944-3.467"
        stroke="#1C1C1C"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export default SearchSVG
