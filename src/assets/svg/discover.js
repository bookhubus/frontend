
import * as React from "react"

function DiscoverSvg(props) {
  const {stroke, name} =props
  return (
    <svg width={45} height={45} viewBox="0 0 45 45" fill="none" {...props}>
      <path
        name = {name}
        d="M22.5 41.25c10.355 0 18.75-8.395 18.75-18.75S32.855 3.75 22.5 3.75 3.75 12.145 3.75 22.5s8.395 18.75 18.75 18.75z"
        stroke={stroke}
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        name = {name}
        d="M30.45 14.55l-3.975 11.925L14.55 30.45l3.975-11.925L30.45 14.55z"
        stroke= {stroke}
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export default DiscoverSvg

