import * as React from "react"

function LockSVG(props) {
  return (
    <svg width={20} height={20} viewBox="0 0 20 20" fill="none" {...props}>
      <path
        d="M15.603 9.167H4.493c-.878 0-1.588.746-1.588 1.666v5.834c0 .92.71 1.666 1.587 1.666h11.111c.877 0 1.588-.746 1.588-1.666v-5.834c0-.92-.711-1.666-1.588-1.666zM6.08 9.167V5.833c0-1.105.418-2.165 1.162-2.946a3.875 3.875 0 012.806-1.22c1.052 0 2.062.439 2.806 1.22a4.275 4.275 0 011.162 2.946v3.334"
        stroke="#161616"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export default LockSVG
