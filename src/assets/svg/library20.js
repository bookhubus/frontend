import * as React from "react"

function Library20SVG(props) {
  return (
    <svg width={20} height={20} viewBox="0 0 20 20" fill="none" {...props}>
      <path
        d="M3.746 16.25c0-.553.209-1.082.581-1.473.372-.39.877-.61 1.403-.61h10.714"
        stroke="#161616"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth = {props.lineWidth}
      />
      <path
        d="M5.73 1.667h10.714v16.666H5.73c-.526 0-1.03-.22-1.403-.61a2.137 2.137 0 01-.581-1.473V3.75c0-.553.209-1.082.581-1.473.372-.39.877-.61 1.403-.61v0z"
        stroke="#161616"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth = {props.lineWidth}
      />
    </svg>
  )
}

export default Library20SVG
