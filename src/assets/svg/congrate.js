import * as React from "react"

function CongrateSVG(props) {
  return (
    <svg width={24} height={24} viewBox="0 0 24 24" fill="none" {...props}>
      <path
        d="M12 15a7 7 0 100-14 7 7 0 000 14z"
        stroke="#161616"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M8.21 13.89L7 23l5-3 5 3-1.21-9.12"
        stroke="#161616"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export default CongrateSVG
