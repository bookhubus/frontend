import * as React from "react"

function HomeSVG(props) {

  const {stroke, name} = props
  return (
    <svg width={33} height={37} viewBox="0 0 45 45" fill="none" {...props}>
      <path
        name = {name}
        d="M5.625 16.875L22.5 3.75l16.875 13.125V37.5a3.75 3.75 0 01-3.75 3.75H9.375a3.75 3.75 0 01-3.75-3.75V16.875z"
        stroke={stroke}
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        name = {name}
        d="M16.875 41.25V22.5h11.25v18.75"
        stroke={stroke}
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export default HomeSVG
