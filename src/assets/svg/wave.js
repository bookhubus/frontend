import * as React from "react"

function WaveSVG(props) {
  return (
    <svg width={389} height={183} viewBox="0 0 389 183" fill="none" {...props}>
      <path
        d="M106.458 112.029c-51.84 0-56.277 54.502-100.578 58.952-3.223.324-5.88 2.914-5.88 6.154A5.865 5.865 0 005.865 183H382a7 7 0 007-7V7c0-3.866-3.191-7.029-7.052-6.821-44.431 2.392-20.064 27.975-67.62 27.975-54.046 0-51.523 41.644-101.412 41.644-54.047 0-56.569 42.231-106.458 42.231z"
        fill="#A6CEE3"
        fillOpacity={0.8}
      />
    </svg>
  )
}

export default WaveSVG
