import * as React from "react"

function LibrarySVG45(props) {
  const {stroke, name} = props

  return (
    <svg width={45} height={45} viewBox="0 0 45 45" fill="none" {...props}>
      <path
        name={name}
        d="M7.5 36.563a4.688 4.688 0 014.688-4.688H37.5"
        stroke={stroke}
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        name={name}
        d="M12.188 3.75H37.5v37.5H12.187A4.688 4.688 0 017.5 36.562V8.439a4.687 4.687 0 014.688-4.688v0z"
        stroke={stroke}
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export default LibrarySVG45
