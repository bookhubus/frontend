import * as React from "react"

function LibrarySVG30(props) {
  return (
    <svg width={30} height={30} viewBox="0 0 30 30" fill="none" {...props}>
      <path
        d="M5 24.375a3.125 3.125 0 013.125-3.125H25"
        stroke="#161616"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M8.125 2.5H25v25H8.125A3.125 3.125 0 015 24.375V5.625A3.125 3.125 0 018.125 2.5v0z"
        stroke="#161616"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export default LibrarySVG30
