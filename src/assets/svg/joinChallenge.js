import * as React from "react"

function JoinChallengeSVG(props) {

  const {stroke, name} = props
  return (
    <svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M16 17.347V15.6949C16 14.8185 15.5786 13.9781 14.8284 13.3585C14.0783 12.7388 13.0609 12.3907 12 12.3907H5C3.93913 12.3907 2.92172 12.7388 2.17157 13.3585C1.42143 13.9781 1 14.8185 1 15.6949V17.347" stroke="#161616" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M8.5 9.08653C10.7091 9.08653 12.5 7.60719 12.5 5.78234C12.5 3.95749 10.7091 2.47815 8.5 2.47815C6.29086 2.47815 4.5 3.95749 4.5 5.78234C4.5 7.60719 6.29086 9.08653 8.5 9.08653Z" stroke="#161616" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M20 6.6084V11.5647" stroke="#161616" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M23 9.08655H17" stroke="#161616" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
    </svg>
 )
}

export default JoinChallengeSVG
