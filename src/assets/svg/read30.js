import * as React from "react"

function ReadSVG30(props) {
  return (
    <svg width={31} height={30} viewBox="0 0 31 30" fill="none" {...props}>
      <path
        d="M3.167 3.75h7.5a5 5 0 015 5v17.5a3.75 3.75 0 00-3.75-3.75h-8.75V3.75z"
        stroke="#000"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M28.166 3.75h-7.5a5 5 0 00-5 5v17.5a3.75 3.75 0 013.75-3.75h8.75V3.75z"
        stroke="#000"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export default ReadSVG30
