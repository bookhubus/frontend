import * as React from "react"

function ClockSVG(props) {
  return (
    <svg width={20} height={20} viewBox="0 0 20 20" fill="none" {...props}>
      <path
        d="M10.095 18.333c4.383 0 7.937-3.73 7.937-8.333 0-4.602-3.554-8.333-7.937-8.333-4.383 0-7.936 3.73-7.936 8.333 0 4.602 3.553 8.333 7.936 8.333z"
        stroke="#161616"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M10.095 5v5l3.175 1.667"
        stroke="#161616"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export default ClockSVG
