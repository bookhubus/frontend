import * as React from "react"

function ListSvg(props) {
  const {stroke, name} =props
  
  return (
    <svg width={45} height={45} viewBox="0 0 45 45" fill="none" {...props}>
      <path
        name = {name}
        d="M30 7.5h3.75a3.75 3.75 0 013.75 3.75V37.5a3.75 3.75 0 01-3.75 3.75h-22.5A3.75 3.75 0 017.5 37.5V11.25a3.75 3.75 0 013.75-3.75H15"
        stroke={stroke}
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        name = {name}
        d="M28.125 3.75h-11.25C15.839 3.75 15 4.59 15 5.625v3.75c0 1.036.84 1.875 1.875 1.875h11.25c1.035 0 1.875-.84 1.875-1.875v-3.75c0-1.036-.84-1.875-1.875-1.875z"
        stroke={stroke}
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export default ListSvg
