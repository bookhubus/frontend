import * as React from "react"

function DarkIcon(props) {
  return (
    <svg width={20} height={20} viewBox="0 0 21 21" fill="none" {...props}>
      <path
        d="M19.875 11.323A9.375 9.375 0 119.677 1.125a7.292 7.292 0 0010.198 10.198v0z"
        stroke="#161616"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export default DarkIcon
