import * as React from "react"

function ListenSVG30(props) {
  return (
    <svg width={31} height={30} viewBox="0 0 31 30" fill="none" {...props}>
      <path
        d="M14.416 6.25l-6.25 5h-5v7.5h5l6.25 5V6.25zM20.092 10.575a6.25 6.25 0 010 8.838m4.412-13.25a12.5 12.5 0 010 17.674V6.163z"
        stroke="#000"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export default ListenSVG30
