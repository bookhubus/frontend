import React, { Component } from 'react';
import './rightCard.css';
import FriendCard from '../friendCard/friendCard';

function RightCard(props) {

    const card = props.card;
    return (
        <div className="card">
            <div className="cardBookImg">
                <img className="rightBookImageCover" src={card.bookImg} />
                <img className="Cover" src={card.bookImg} />
            </div>
            <div className="cardDetails">
                <h5 className="bookAuthor">{card.Author}</h5>
                <h3 className="bookName">{card.bookName}</h3>
                <p>{card.description}</p>
                <FriendCard friends={card.friends} friendsNumber={card.friendsNumber} className={"rightFriendCard"} />
            </div>
        </div>
    )

}

export default RightCard;
