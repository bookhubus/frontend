import React from 'react';
import Gallery from 'react-photo-gallery';
function DiscoverHorizontalList(props) {
  const books = props.books;
  return (
    //<BooksList books={books} bookStyle={"bookCover"} />
    <div style={{marginRight:"40px"}}>
      <Gallery photos={books} />

    </div>
  )
}

export default DiscoverHorizontalList;