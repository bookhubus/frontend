import React, { Component } from 'react';
import { Circle } from 'rc-progress'
import './progressIconStyle.css'

class ProgressIcon extends Component {

    render(props) {
        return (

            <a href = "" className="friendContainer">
                <img src={this.props.img} className="friendImg" />
                <Circle
                    percent={this.props.percentage}
                    strokeWidth="3"
                    strokeColor="#05f16e"
                    trailColor="#a4a4a4"
                    style={{ position: "absolute" }} />
            </a>
        )
    }
}

export default ProgressIcon;
