import React, { Component } from 'react';
import './discoverBookCard.css';
import LibrarySVG30 from '../../assets/svg/library30';
import ReadSVG30 from '../../assets/svg/read30'
import ListenSVG30 from '../../assets/svg/listen30';
class DiscoverBookCard extends Component {
    
    render(props) { 
       
        return( 
            <section className = "cardContainer">
                <img src = {this.props.bookImg} className = "bookContainer"/>
                <div className = "infoContainer">
                    <div className = 'cardHeader'>
                        <div className = "text">
                            {this.props.bookName}
                        </div>
                        <a className = "iconContainer">
                            <LibrarySVG30/>
                        </a>
                    </div>
                    <div className = "text" style = {{fontSize: "22px", lineHeight: "32px"}}>
                        {this.props.Author}
                    </div>
                    <div className = "text" style = {{fontSize: "22px", lineHeight: "32px"}}>
                        {this.props.publishDate}
                    </div>
                    <div className = "text" style = {{fontSize: "22px", lineHeight: "32px"}}>
                        {this.props.genre}
                    </div>
                    <div className = "text" style = {{fontSize: "22px", lineHeight: "32px"}}>
                        {this.props.part}
                    </div>
                    <div className = "footerContainer">
                        <a href = {this.props.readLink}  className = "actionContainer">
                            <div className = "iconContainer" style = {{marginLeft: 0}}>
                                <ReadSVG30/>
                            </div>
                            <div className = "text">
                                Read
                            </div>
                        </a>
                        <a  href = {this.props.listenLink} className = "actionContainer" style = {{marginLeft: "50px", width:"115px"}}>
                            <div className = "iconContainer" style = {{marginLeft: 0}}>
                                <ListenSVG30/>
                            </div>
                            <div className = "text">
                                Listen
                            </div>
                        </a>
                        <div className = "buttonContainer">
                            <div className = "text">
                                {this.props.price}
                            </div>
                            <a href = {this.props.buyLink} className = "buy">
                                <div className = "text">
                                Buy

                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </section>

          
        )
    }
}
 
export default DiscoverBookCard;
