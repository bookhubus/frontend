import React, { Component } from 'react';
import './postStyle.css';
import LockSVG from '../../assets/svg/lock'
import ClockSVG from "../../assets/svg/clock"
import Library20SVG from "../../assets/svg/library20"


class Post extends Component {

    render(props) {

        return (
            <div className="postContainer">
                <div className="postHeaderContainer">
                    <img className="creatorImg" src={this.props.postCreator} />
                    <div className="headerTextContainer">
                        <div className="postText">
                            {this.props.postText}
                        </div>

                        <div className="postText"
                            style={{ lineHeight: "19px", fontSize: "12px", fontWeight: "lighter" }}>
                            {this.props.postDate} ago
                        </div>
                    </div>

                </div>

                {this.props.hasChallengeInfo
                ?<div className="postChallenge">
                    <div className="postText" style={{ marginTop: "10px" }}>
                        {this.props.challengeName}
                    </div>
                    <div className="challengeInfo">
                        <div className="info" style={{ width: "25%" }}>
                            <LockSVG style={{ marginRight: "3%" }} />
                            <div className="postText"
                                style={{ fontSize: "16px", lineHeight: "23px", fontWeight: "lighter" }}
                            >{this.props.challengePrivacy}</div>
                        </div>
                        <div className="info" style={{ marginRight: "5px" }}>
                            <ClockSVG style={{ marginRight: "3%" }} />
                            <div className="postText"
                                style={{ fontSize: "16px", lineHeight: "23px", fontWeight: "lighter" }}>
                                {this.props.challengeTimeLeft} left</div>
                        </div>
                        <div className="info">
                            <Library20SVG style={{ marginRight: "3%" }} />
                            <div className="postText"
                                style={{ fontSize: "16px", lineHeight: "23px", fontWeight: "lighter" }}
                            >{this.props.bookNum} book</div>
                        </div>
                    </div>
                </div>
                :
                <div/>
                }
                <div className = "challengeFooter">
                    <div className = "firstStatContainer">
                        {this.props.footerIcon}
                        <div className = "postText"
                            style = {{fontSize: "18px",lineHeight: "32px",marginLeft:"5px"}}>
                            {this.props.footerText}
                        </div>
                    </div>
                    {this.props.hasSecondState
                    ?<div className = "secondContainer">
                        {this.props.footerIcon2}
                        <div className = "postText"
                            style = {{fontSize: "18px",lineHeight: "32px",marginLeft:"5px"}}>
                            {this.props.footerText2}
                        </div>
                    </div>
                    :<div/>
                    }
                </div>

            </div>
        )
    }
}

export default Post;
