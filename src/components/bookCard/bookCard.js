import React, { Component, useState } from 'react';
import './bookCardStyle.css'
import { Line } from 'rc-progress'
import ReactStars from "react-rating-stars-component";
import EditSVG from "../../assets/svg/editIcon"
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import CreateChallenge from '../../components/forms/CreateChallenge'
import { gql, useQuery, useMutation } from '@apollo/client';
import { useHistory } from 'react-router-dom';

const setBookProgressMutation = gql`mutation setBookProgress($input: SetBookProgressInput!) {
    setBookProgress(input: $input)
    {
      book
      {
        id
      }
    }
  }`;

const BookCard = (props) => {
    const [showInfo, setShowInfo] = useState(false);
    const [rating, setRating] = useState('');
    const [show, setShow] = useState(false);
    const [pageNumber, setPageNumber] = useState(props.currentPage);
    const [percent, setPercent] = useState(props.percent)

    const [setBookProgress, { loading, error, data }] = useMutation(setBookProgressMutation);

    let history = useHistory();

    const handleBookProgress = React.useCallback(async () => {
        let fixedPageNum = parseInt(pageNumber);
        if(parseInt(pageNumber) > props.maxPages)
        {
            fixedPageNum = props.maxPages;
            setPageNumber(props.maxPages);
        }
        const data = await setBookProgress({
            variables: {
                input: {
                    bookId: props.bookId,
                    pageNumber: fixedPageNum
                }
            }
        });

        if(data)
        {
            setPercent(Math.round(fixedPageNum/props.maxPages * 100));
        }
    });

    const handleClose = React.useCallback(async () => {
        setShow(false);
    })
    const handleShow = React.useCallback(async () => {
        setShow(true);
    })
    const showInfoF = React.useCallback(async () => {
        setShowInfo(!showInfo)
        if(!props.hasInfo)
        {
            history.push("/bookinfo/" + props.bookId);
        }
    })
    const ratingChanged = React.useCallback(async (newRating) => {
        setRating(newRating)
    })

    return (
        <div className="homeCardContainer">
            <img
                onClick={showInfoF}
                src={props.bookImg} className="cover"
                style={{ cursor: "pointer" }}
            />
            {props.hasInfo
                ? <div className={showInfo ? "expand" : "collapse1"}>
                    <div className="header">
                        <div className="progressText" style={{ fontWeight: "normal", fontSize: "16px" }}>
                            {props.genere}
                        </div>
                        <div style={{ marginTop: "15px", marginLeft: "30px" }}>
                            <ReactStars
                                count={5}
                                onChange={ratingChanged}
                                size={23}
                                activeColor="#ffd700"
                            />,
                            </div>
                        <div className="progressText">
                            {props.rating}
                        </div>
                    </div>
                    <div className="bookNameContainer">
                        <a href= {"bookinfo/" + props.bookId} className="progressText" style={{ margin: 0, cursor: "pointer" }}>
                            {props.bookName}
                        </a>
                    </div>
                    <div className="percentage">
                        <div className="progressText" style={{ fontSize: "16px" }}>
                            <span>{percent}%</span>
                            </div>
                    </div>
                    <div style={{ width: "98%", height: "20px", marginTop: "-5px", display: "flex", flexDirection: "row" }}>

                        <div className="progressBar">
                            <Line percent={percent} strokeWidth="1" strokeColor=" #17BF63" />
                        </div>
                        <button
                            onClick={handleShow}
                            style={{ marginTop: "-15px", marginLeft: "10px", border: "none", backgroundColor: "transparent" }}>
                            <EditSVG />
                        </button>
                    </div>
                    <Modal show={show} onHide={handleClose}>
                        <Modal.Header closeButton>
                            <h2>Reading Progress</h2>
                        </Modal.Header>
                        <div style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
                            <div className="text" style={{ display: "flex", marginTop: "30px", marginBottom: "30px", marginLeft: "20px" }}>
                                Page Number
                                </div>
                            <input
                                type="number"
                                min="0"
                                max={props.maxPages}
                                value={pageNumber}
                                onChange={(e) => setPageNumber(e.target.value)}
                                style={{ height: "20px", width: "60px", marginLeft: "170px" }}>
                            </input>

                            <div className="text" style={{ marginLeft: "10px", marginRight: "10px" }}>
                                /
                                </div>
                            <div className="infoFeild">
                                {props.maxPages}
                                </div>
                        </div>
                        <span style={{marginLeft: "5%", fontSize: "14px"}}>*Page numbers above the max will be set to it.</span>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={() => {handleBookProgress();handleClose()}}>
                                Save
                            </Button>

                        </Modal.Footer>
                    </Modal>
                </div>
                : <div />}
        </div>
    )
}

export default BookCard;
