import React, { Component } from 'react';
import './bookListStyle.css'
import BookCard from '../bookCard/bookCard';
import ChallengeCard from '../challengeCard/challengeCard';


class BookList extends Component {

  constructor() {
    super()
    this.state = {
      leftShadow: false,
      rightShadow: true
    }

  }
  handleScroll = (e) => {
    const right = e.target.scrollWidth - e.target.scrollLeft === e.target.clientWidth;
    const left = e.target.scrollLeft === 0
    if (right) {
      this.setState({
        rightShadow: false
      })
    }
    else if (left) {
      this.setState({
        leftShadow: false
      })
    }
    else {
      this.setState({
        leftShadow: true,
        rightShadow: true
      })
    }
  }
  render() {
    var items = this.props.cards;
    var listItems = items.map((card, index) =>
      (this.props.isBookCard ?
        <BookCard
          key={index}
          bookId={card.bookId}
          hasInfo={this.props.hasInfo}
          bookImg={card.bookImg}
          genere={card.genere}
          rating={card.rating}
          bookName={card.bookName}
          percent={card.percent}
          currentPage={card.currentPage}
          maxPages={card.maxPages}
        />
        :
        <ChallengeCard
          key={index}
          challengeName={card.challengeName}
          privacy={card.privacy}
          timeLeft={card.timeLeft}
          booksNum={card.booksNum}
          friendImg1={card.friendImg1}
          percentage1={card.percentage1}
          friendImg2={card.friendImg2}
          percentage2={card.percentage2}
          friendImg3={card.friendImg3}
          percentage3={card.percentage3}
          friendImg4={card.friendImg4}
          percentage4={card.percentage4}
        />
      )
    );
    return (
      <div >
        <ul className="listContainer" onScroll={this.handleScroll}>{listItems}
        </ul>
        {!this.state.leftShadow &&
          <div className="listShadow l"></div>}
        {!this.state.rightShadow &&
          <div className="listShadow r"></div>}
        {this.state.leftShadow && this.state.rightShadow &&
          <div className="listShadow"></div>}
      </div>
    )
  }
}

export default BookList;
