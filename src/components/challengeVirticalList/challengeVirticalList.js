import React from 'react';
import ChallengeCard from '../challengeCard/challengeCard'

function ChallengeVirticalList(props) {
  const challenges = props.challenges;
  const listItems = challenges.map((card, index) =>
    <div style={{marginBottom: "50px"}}>
      <ChallengeCard
        key={index}
        challengeName={card.challengeName}
        privacy={card.privacy}
        timeLeft={card.timeLeft}
        booksNum={card.booksNum}
        friendImg1={card.friendImg1}
        percentage1={card.percentage1}
        friendImg2={card.friendImg2}
        percentage2={card.percentage2}
        friendImg3={card.friendImg3}
        percentage3={card.percentage3}
        friendImg4={card.friendImg4}
        percentage4={card.percentage4}
        withJoinButton={true}
      />
    </div>

  );
  return (
    <ul className={props.listContainer}>{listItems}</ul>
  )
}

export default ChallengeVirticalList;