import React, { Component } from 'react';
import './leftCard.css';
import '../friendCard/friendCard'
import FriendCard from '../friendCard/friendCard';

function LeftCard(props) {

    const card = props.card;
    return (
        <div className="card">
            <div className="cardDetails">
                <h5 className="bookAuthor">{card.Author}</h5>
                <h3 className="bookName">{card.bookName}</h3>
                <p>{card.description}</p>
                <FriendCard friends={card.friends} friendsNumber={card.friendsNumber} className={"leftFriendCard"}/>
            </div>
            <div className="cardBookImg">
                <img className="leftBookImageCover" src={card.bookImg} />
                <img className="Cover" src={card.bookImg} />
            </div>
        </div>
    )

}

export default LeftCard;
