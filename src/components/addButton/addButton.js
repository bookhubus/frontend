import React, { Component } from 'react';
import './addButton.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'

class AddButton extends Component {

    render(props) {
        const href = this.props.href;
        return (
            <div class="fixed-action-btn">
                <button onClick= {this.props.onClick} class="btn-floating btn-large red">
                    <FontAwesomeIcon icon={faPlus} />
                </button>
            </div>
        )
    }
}

export default AddButton;
