import React, { Component } from 'react';
import './friendCard.css';
function FriendCard(props) {

    const friends = props.friends;
    const listFriends = friends.map((friend) =>
        <img src={friend} className="friendImage" />
    )

    return (
        <div className={props.className}>
            <p className="friendsNumber">{props.friendsNumber} Friends Read This</p>
            {listFriends}
        </div>
    )

}

export default FriendCard;
