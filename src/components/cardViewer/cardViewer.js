import React, { Component } from 'react';
import './cardViewer.css';
import LeftCard from '../leftCard/leftCard'
import RightCard from '../rightCard/rightCard'
function CardViewer(props) {

    const cards = props.cards;
    const listItems = cards.map((card, index) => {
        if (index % 2 == 0) {
            return (
                <view>
                    <LeftCard card={card} />
                    <br></br>
                </view>
            )
        } else {
            return (
                <view>
                    <RightCard card={card} />
                    <br></br>
                </view>
            )
        }
    })

    return (
        <ul className={props.listContainer}>{listItems}</ul>
    )

}

export default CardViewer;
