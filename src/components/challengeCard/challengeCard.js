import React, { Component } from 'react';
import './challengeCardStyle.css'
import WaveSVG from '../../assets/svg/wave'
import Settings20SVG from '../../assets/svg/settings20'
import ClockSVG from '../../assets/svg/clock'
import LockSVG from '../../assets/svg/lock'
import Library20SVG from '../../assets/svg/library20.js'
import ProgressIcon from '../progressIcon/progressIcon';
import ArrowRightCircleSVG from '../../assets/svg/arrow_right_circle.js';

class ChallengeCard extends Component {

    render(props) {

        return (
            <div className="challengeCardContainer">
                <WaveSVG className="waveStyle" />
                <div className="challengeInfoContainer">
                    <div className="nameAndSettings">
                        <div className="challengeName"
                            style={{
                                fontWeight: "bold",
                                fontSize: "28px",
                                lineHeight: "40px",
                                marginLeft: "9.5px"
                            }}>
                            {this.props.challengeName}
                        </div>
                        <a style={{ margin: 0, marginRight: "6.8%" }}>
                            <Settings20SVG />
                        </a>
                    </div>
                    <div className="challengeDetails">
                        <div className="infoField" style={{ marginLeft: "2.5%", width: "25%" }}>
                            <LockSVG style={{ marginRight: "3%" }} />
                            <div className="challengeName">{this.props.privacy}</div>
                        </div>
                        <div className="infoField">
                            <ClockSVG style={{ marginRight: "3%" }} />
                            <div className="challengeName">{this.props.timeLeft} day(s) left</div>
                        </div>
                        <div className="infoField">
                            <Library20SVG style={{ marginRight: "3%" }} />
                            <div className="challengeName">{this.props.booksNum} book</div>
                        </div>
                    </div>
                    <div className="challengeName" style={{ marginLeft: "2.5%", marginTop: "8%", fontWeight: "bold" }}>
                        Friends
                    </div>
                    <div className="challengeMembers">
                        <ProgressIcon img={this.props.friendImg1} percentage={this.props.percentage1} />
                        <ProgressIcon img={this.props.friendImg2} percentage={this.props.percentage2} />
                        <ProgressIcon img={this.props.friendImg3} percentage={this.props.percentage3} />
                        <ProgressIcon img={this.props.friendImg4} percentage={this.props.percentage4} />
                        {
                            (this.props.withJoinButton) ?
                                <div className="joinChallenge">
                                    <a href="/join">
                                        <ArrowRightCircleSVG />
                                    </a>
                                </div>
                                :
                                <div>
                                </div>
                        }
                    </div>
                </div>
            </div>
        )
    }
}

export default ChallengeCard;
