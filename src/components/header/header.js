import './header.css'
import user from '../../assets/image/user.jpg';
import SearchSVG from '../../assets/svg/search.js';
import React, { Component } from 'react';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchText: ""
        }
        this.search = this.search.bind(this)
    }
    search(text) {
        this.setState({
            searchText: text
        })
        
    }
    render() {
        return (
            <div className="topBar">
                <div className="topBarUser">
                    <a href="" style={{ margin: 0 }}>
                        <img src={localStorage.getItem("image")} className="userAvatar" />
                    </a>
                    <a href="" className="userName">Hi, {localStorage.getItem("name")}</a>
                </div>
                <div className="searchField">
                    <SearchSVG className="searchIcon" />
                    <input
                        onChange={(value) => this.search(value)}
                        className="inputSearch"
                        placeholder="Search"
                        value={this.searchText} />
                </div>
            </div>
        )
    }
}
export default Header