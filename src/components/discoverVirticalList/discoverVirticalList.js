import React from 'react';
import DiscoverBookCard from '../discoverBookCard/discoverBookCard'
import Post from '../post/post';
import Library24SVG from '../../assets/svg/library24';
import JoinChallengeSVG from '../../assets/svg/joinChallenge'
import ReadBookSVG from '../../assets/svg/readBook'
import CongrateSVG from '../../assets/svg/congrate'

function DiscoverVirticalList(props) {
    const items = props.items;
    const listItems = items.map((item, index) =>
      (props.isBookCard ?
      <DiscoverBookCard key = {index}
        bookImg = {item.bookImg}
        bookName = {item.bookName}
        Author = {item.Author}
        publishhDate = {item.publishhDate}
        genre = {item.genre}
        price = {item.price}
        part = {item.part}
        readLink = {item.readLink}
        listenLink = {item.listenLink}
        buyLink = {item.buyLink}/>
        :
        <Post key = {index}
        postCreator={item.img}
        postText={item.postText}
        postDate={item.postDate}
        hasChallengeInfo={item.hasChallenge}
        challengeName={item.challengeName}
        challengePrivacy={item.challengePrivacy}
        challengeTimeLeft={item.challengeTime}
        bookNum={item.bookNum}
        footerIcon={item.footerIcon=="suggest"?<Library24SVG/>:
          item.footerIcon=="joinChallenge"?<JoinChallengeSVG/>:
          <ReadBookSVG/>}
        footerText={item.footerText}
        hasSecondState={item.hasSecondState}
        footerIcon2={item.hasSecondState?<CongrateSVG />:''}
        footerText2={item.footerText2}
          />
    )
    );
    return (
      <ul className = {props.listContainer}>{listItems}</ul>
    )
}

export default DiscoverVirticalList;