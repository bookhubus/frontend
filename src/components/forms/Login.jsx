import React, { useState } from 'react';
import { useHistory } from "react-router-dom";
import { gql, useMutation } from '@apollo/client';
import './css/forms.css'

const EXCHANGE_RATES = gql`
  mutation SignIn($input: SigninInput!) {
    signin(input: $input) {
        accessToken
        user {
            id
            name
            image
        }
    }
  }
`;

const Login = () => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [message, setMessage] = useState('')
    const [correctPassword, setCorrectPassword] = useState(true)

    let history = useHistory();

    const [SignIn, { loading, error, data }] = useMutation(EXCHANGE_RATES);


    const handleSubmit = React.useCallback(async (event) => {
        //submit the form to an API here+
        event.preventDefault()
        //if the password did not match setState with CorrectPassword wrong and message Wrong password
        const data = await SignIn({
            variables: {
                input: {
                    authProvider: {
                        provider: "EMAIL",
                        email: email,
                        password: password
                    }
                }
            }
        })
        console.log("WWWssssssss", data);
        localStorage.setItem('userToken', data.data.signin.accessToken)
        localStorage.setItem('name', data.data.signin.user.name)
        localStorage.setItem('image', data.data.signin.user.image)
        history.replace({ pathname: "/" });

    })

    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;



    return (
        <div
            className="container"
            style={{ height: '13rem' }}
        >
            <form
                className="form"
                onSubmit={handleSubmit}
            >
                <nav className="navBar" >Sign in</nav>
                <input
                    placeholder="Email Address"
                    type="text"
                    className="textInput"
                    onChange={event => setEmail(event.target.value)}
                    required={true}
                    name="name"
                    value={email}
                />
                <br />
                <input
                    placeholder={"Password"}
                    style={correctPassword ? {} : { borderColor: 'red' }}
                    type="password"
                    className="textInput"
                    minLength="8"
                    name="password"
                    value={password}
                    onChange={event => setPassword(event.target.value)}
                    required={true}
                />
                <br />
                <span className="Alert">{message}</span>
                <button className="btn-grad">Sign in</button>
            </form>
            <br />
            <div className="labelStyle">
                <label style={{ fontSize: 'small' }}>New to BookHub?</label>
                <a className="attribute" href="Google.com">Sign Up</a>
            </div>
        </div>
    );
}

export default Login;