import React, { Component } from 'react';
import DatePicker from "react-datepicker";
import './css/forms.css'

class CreateChallenge extends Component {
    constructor()
    {
        super()
        this.state = 
        {
            name: "",
            number: "",
            date: new Date()
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleChange(event)
    {
        const {name, value,} = event.target
        this.setState(
            {
                [name]: value,
            }
        )
    }
    handleSubmit(event)
    {
        //submit the form to an API here+
        const {password, confirmPassword} = this.state
    }
    render() { 
        const { name, number, date} = this.state
        return ( 
            <div > 
            <form 
                style={{justifyContent: 'center', display: 'flex', flexDirection: 'column'}}
                onSubmit = {this.handleSubmit}
            >
                    <input 
                        className ="textInput2"
                        style = {{marginTop: '20px'}}
                        placeholder="Challenge name" 
                        type="text"
                        onChange={this.handleChange}
                        required={true}
                        name = "name" 
                        value = {name}
                    />
                <br />
                <input 
                    className ="textInput2"
                    placeholder="Duration in days"
                    type="number"
                    value = {number}
                    onChange={ this.handleChange }
                    required={true}
                    name = "number"
                    min = "0"
                />
                <br/>
                <input 
                    className ="textInput2"
                    type="date"
                    onChange={this.handleChange}
                    required={true}
                    name = "date" 
                    value = {date}
                />
                <div >
                <label style={{fontSize: 'small', fontFamily: 'sans-serif',padding: '5px', marginLeft:"20px", marginRight:"10px"}}>Note: This is the start date of the challenge!</label>
                </div>
                <label>
                    <input style = {{marginLeft:"20px", marginRight:"5px"}} type="checkbox" className="toggle" />
                      Private Challenge
                </label>
            </form>
        </div>
         );
    }
}
 
export default CreateChallenge;