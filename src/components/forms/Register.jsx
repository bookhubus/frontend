import React, { Component } from 'react'
import { useHistory } from "react-router-dom";
import './css/forms.css'
import { useState } from 'react'
import { gql, useMutation } from '@apollo/client'

const graphQLQuery = gql`mutation SignUp($input: SignupInput!){
    signup(input: $input)
    {
      accessToken
      user
      {
        name
        username
        id
      }
    }
  }`;

const Register = () => {

    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const [message, setMessage] = useState('')
    const [match, setMatch] = useState(true)

    const [SignUp, {loading, error, data}] = useMutation(graphQLQuery);

    let history = useHistory();

    const handleSubmit = React.useCallback(async (event) => {
        //submit the form to an API here+
        event.preventDefault()

        if (password !== confirmPassword) {
            setMatch(false)
            setMessage("Password didn't match")
        }
        else {
            setMatch(true)
            setMessage("")

            SignUp(
                {
                    variables: {
                        input: {
                            name: name,
                            username: email,
                            authProvider: {
                                provider: "EMAIL",
                                email: email,
                                password: password
                            }
                        }
                    }
                }
            ).then((data) => {
                localStorage.setItem("userToken", data.data.signup.accessToken)
                localStorage.setItem("name", name);
                history.replace({ pathname: "/" });
            }).catch((err) => {
                setMessage("Email already used. Please try another Email.");
            })
        }
    })

    return (
        <div className="container">
            <form
                className="form"
                onSubmit={handleSubmit}
            >
                <nav className="navBar" >Sign Up</nav>
                <input
                    placeholder="Name"
                    type="text"
                    className="textInput"
                    onChange={event => setName(event.target.value)}
                    required={true}
                    name="name"
                    value={name}
                />
                <br />
                <input
                    placeholder="Email"
                    className="textInput"
                    type="email"
                    name="email"
                    value={email}
                    onChange={event => setEmail(event.target.value)}
                    required={true}
                />
                <br />
                <input
                    placeholder={"Password"}
                    style={match ? {} : { borderColor: 'red' }}
                    type="password"
                    className="textInput"
                    minLength="8"
                    name="password"
                    value={password}
                    onChange={event => setPassword(event.target.value)}
                    required={true}
                />
                <br />
                <input
                    placeholder="Confirm Password"
                    style={match ? {} : { borderColor: 'red' }}
                    type="password"
                    className="textInput"
                    name="confirmPassword"
                    value={confirmPassword}
                    onChange={event => setConfirmPassword(event.target.value)}
                    required={true}
                />
                <br />
                <span className="Alert">{message}</span>
                <button className="btn-grad">Sign Up</button>
            </form>
            <br />
            <div className="labelStyle">
                <label style={{ fontSize: 'small' }}>Already a member?</label>
                <a className="attribute" href="Google.com">Sign In</a>
            </div>
        </div>
    )
}
export default Register