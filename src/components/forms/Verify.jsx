import React, { Component } from 'react';
import './css/forms.css'
class Verify extends Component {
    constructor(props)
    {
        super(props)
        this.state = 
        {
            pageType: this.props.pageType,
            value: ''
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleChange(event)
    {
        const { value }  = event.target
        this.setState(
            {
                value: value,
            }
        )
    }
    handleSubmit(event)
    {
        //submit the form to an API here+
        event.preventDefault()
        //if the password did not match setState with CorrectPassword wrong and message Wrong password
    }
    render() { 
        const { value, pageType } = this.state
        return ( 
            <div 
                className= 'container'
                style={{height: '8rem'}}
            >
                <form 
                    className= 'form'
                    onSubmit = {this.handleSubmit}
                >
                    <nav className= 'navBar'>{pageType}</nav>
                    <input 
                        className= "textInput" 
                        type= 'text'
                        placeholder= 'Code'
                        required= {true}
                        name= 'code'
                        value={ value }
                        onChange={this.handleChange}
                    />
                    <br/>
                    <button className= "btn-grad">Confirm</button>
                </form>
            </div>
         );
    }
}
 
export default Verify;