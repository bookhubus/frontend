import React from 'react';
import './App.css';
import Discover from './screens/Discover/index'
import Library from './screens/Library/index'
// import Lists from './screens/Lists/index'
import Home from './screens/Home/index'
import Challenge from './screens/Challenge/index'
import Profile from './screens/Profile/index'
import Login from '../src/components/forms/Login'
import Register from '../src/components/forms/Register'
import CreateChallenge from '../src/components/forms/CreateChallenge'
import BookInfo from './screens/BookInfo/index'


import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import { ApolloClient, InMemoryCache, ApolloProvider, createHttpLink } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';

const httpLink = createHttpLink({
  uri: 'https://bookhubus-dev.vercel.app',
});

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = localStorage.getItem('userToken');
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : "",
    }
  }
});


const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache()
});


const App = () => {

  return (
    <ApolloProvider client={client}>
      <Router>
        <Switch>
          <Route path="/login">
            <Login pageName='login' />
          </Route>
          <Route path="/register">
            <Register pageName='register' />
          </Route>
          <Route path="/challenge">
            <Challenge pageName='challenge' />
          </Route>
          {/* <Route path="/lists">
            <Lists pageName='list' />
          </Route> */}
          <Route path="/library">
            <Library pageName='library' />
          </Route>
          <Route path="/discover">
            <Discover pageName='discover' />
          </Route>
          <Route path="/profile">
            <Profile pageName='profile' />
          </Route>
          <Route path="/bookinfo/:id">
            <BookInfo pageName='bookinfo' />
          </Route>
          <Route path="/">
            <Home pageName='home' />
          </Route>
        </Switch>
      </Router>
    </ApolloProvider>
  )
}

export default App;
